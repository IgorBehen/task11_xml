package com.ihorbehen.Model;

public class CandieGroup {
  private int id;
  private String name;

  public CandieGroup() {
  }

  public CandieGroup(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "CandieGroup{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
