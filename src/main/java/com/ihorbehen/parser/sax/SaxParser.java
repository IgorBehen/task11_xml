package com.ihorbehen.parser.sax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.ihorbehen.Model.Candie;
import com.ihorbehen.xmlValidator.XmlValidator;
import org.xml.sax.SAXException;

public class SaxParser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Candie> parse(File xml, File xsd) {
    List<Candie> candies = new ArrayList<>();
    try {
      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
      saxParserFactory.setValidating(true);

      SAXParser saxParser = saxParserFactory.newSAXParser();
      System.out.println(saxParser.isValidating());
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(xml, saxHandler);
      candies = saxHandler.getCandieList();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }
    return candies;
  }
}