package com.ihorbehen.comparator;

import com.ihorbehen.Model.Candie;

import java.util.Comparator;

public class CandieComparator implements Comparator<Candie> {
  @Override
  public int compare(Candie o1, Candie o2) {
    return o1.getCandieId() - o2.getCandieId();
  }
}
