package com.ihorbehen.parser.dom;

import java.util.ArrayList;
import java.util.List;

import com.ihorbehen.Model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMParser {

    public static List<Candie> parse(Document document) {
        List<Candie> candies = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("candie");
        for (int index = 0; index < nodeList.getLength(); index++) {
            Candie candie = new Candie();
            Value value;
            Type type;
            List<Ingredient> ingredients;

            Node node = nodeList.item(index);

            Element element = (Element) node;
            candie.setCandieId(Integer.parseInt(element.getAttribute("candieId")));
            candie.setName(element.getElementsByTagName("name").item(0).getTextContent());
            candie.setProduction(element.getElementsByTagName("production").item(0).getTextContent());
            candie.setEnergy(Integer.parseInt(element.getElementsByTagName("energy").item(0)
                    .getTextContent()));

            value = getValue(element.getElementsByTagName("value"));
            candie.setValue(value);

            type = getType(element.getElementsByTagName("types"));
            candie.setType(type);

            ingredients = getIngredients(element.getElementsByTagName("ingredients"));
            candie.setIngredients(ingredients);

            candies.add(candie);
        }
        return candies;
    }

    private static Type getType(NodeList nodeList) {
        Type type = new Type();
        CandieGroup candieGroup = new CandieGroup();

        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            type.setCaramel(Boolean.parseBoolean(element.getElementsByTagName("caramel").item(0)
                    .getTextContent()));
            type.setIris(Boolean.parseBoolean(element.getElementsByTagName("iris").item(0)
                    .getTextContent()));
            type.setChocolate(Boolean.parseBoolean(element.getElementsByTagName("chocolate").item(0)
                    .getTextContent()));
            type.setStuffed(Boolean.parseBoolean(element.getElementsByTagName("isStuffed").item(0)
                    .getTextContent()));

            Element candieGroupElement = (Element) element.getElementsByTagName("cendieGroup").item(0);
            candieGroup.setId(Integer.parseInt(candieGroupElement.getAttribute("id")));
            candieGroup.setName(candieGroupElement.getTextContent());
            type.setCandieGroup(candieGroup);
        }
        return type;
    }

    private static Value getValue(NodeList nodeList) {
        Value value = new Value();
        CandieGroup candieGroup = new CandieGroup();

        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);

            value.setProteins(Integer.parseInt(element.getElementsByTagName("proteins")
                    .item(0).getTextContent()));
            value.setFats(Integer.parseInt(element.getElementsByTagName("proteins")
                    .item(0).getTextContent()));
            value.setCarbohydrates(Integer.parseInt(element.getElementsByTagName("proteins")
                    .item(0).getTextContent()));

            Element candieGroupElement = (Element) element.getElementsByTagName("cendieGroup").item(0);
            candieGroup.setId(Integer.parseInt(candieGroupElement.getAttribute("id")));
            candieGroup.setName(candieGroupElement.getTextContent());
            value.setCandieGroup(candieGroup);
        }
        return value;
    }


    private static List<Ingredient> getIngredients(NodeList nodes) {
        List<Ingredient> ingredients = new ArrayList<>();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) node;
                    ingredients.add(new Ingredient(el.getTextContent()));
                }
            }
        }
        return ingredients;
    }
}
