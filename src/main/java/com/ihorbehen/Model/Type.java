package com.ihorbehen.Model;

public class Type {
  private CandieGroup candieGroup;
  private boolean caramel;
  private boolean iris;
  private boolean chocolate;
  private boolean isStuffed = false;

  public Type(CandieGroup candieGroup, boolean caramel, boolean iris, boolean chocolate, boolean isStuffed) {
    this.candieGroup = candieGroup;
    this.caramel = caramel;
    this.iris = iris;
    this.chocolate = chocolate;
    this.isStuffed = isStuffed;
  }

  public Type() {

  }

  public CandieGroup getCandieGroup() {
    return candieGroup;
  }

  public void setCandieGroup(CandieGroup candieGroup) {
    this.candieGroup = candieGroup;
  }

  public boolean isCaramel() {
    return caramel;
  }

  public void setCaramel(boolean caramel) {
    this.caramel = caramel;
  }

  public boolean isIris() {
    return iris;
  }

  public void setIris(boolean iris) {
    this.iris = iris;
  }

  public boolean isChocolate() {
    return chocolate;
  }

  public void setChocolate(boolean chocolate) {
    this.chocolate = chocolate;
  }

  public boolean isStuffed() {
    return isStuffed;
  }

  public void setStuffed(boolean stuffed) {
    isStuffed = stuffed;
  }

  @Override
  public String toString() {
    return "Type{" +
        "candieGroup=" + candieGroup.toString() +
        ", caramel=" + caramel +
        ", iris=" + iris+
        ", cooler=" + chocolate +
        ", isStuffed=" + isStuffed +
        '}';
  }
}
