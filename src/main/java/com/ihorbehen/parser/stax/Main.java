package com.ihorbehen.parser.stax;

import java.io.File;

public class Main {

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\xml\\candies.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\candiesXSd.xsd");
    StaxParser staxParser = new StaxParser();
    System.out.println(staxParser.parse(xmlFile));
  }

}
