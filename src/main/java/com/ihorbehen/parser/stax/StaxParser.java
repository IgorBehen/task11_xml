package com.ihorbehen.parser.stax;

import com.ihorbehen.Model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxParser {

  static final String CANDIE_TAG = "candie";
  static final String NAME_TAG = "name";
  static final String PRODUCTION_TAG = "production ";
  static final String VALUE_TAG = "value";
  static final String ENERGY_TAG = "energy";
  static final String TYPES_TAG = "types";
  static final String CARAMEL_TAG = "caramel";
  static final String CANDIE_GROUP_TAG = "candieGroup";
  static final String IRIS_TAG = "iris";
  static final String CHOCOLATE_TAG = "chocolate";
  static final String INGREDIENTS_TAG = "ingredients";
  static final String ISSTUFFED_TAG = "isStuffed";
  static final String CANDIE_ID_ATTRIBUTE = "candieId";
  static final String CANDIE_GROUP_ID_ATTRIBUTE = "id";
  static final String PROTEINS_TAG = "proteins";
  static final String FATS_TAG  = "fats";
  static final String CARBOHYDRATES_TAG  = "carbohydrates";

  private List<Candie> candies = new ArrayList<>();
  private Candie candie;
  private Type type;
  private Value value;
  private CandieGroup candieGroup;
  private List<Ingredient> ingredients;

  private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

  public List<Candie> parse(File xml) {
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case CANDIE_TAG: {
              candie = new Candie();
              Attribute candieId = startElement.getAttributeByName(new QName(CANDIE_ID_ATTRIBUTE));
              if (candieId != null) {
                candie.setCandieId(Integer.parseInt(candieId.getValue()));
              }
              break;
            }
            case NAME_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              candie.setName(xmlEvent.asCharacters().getData());
              break;
            }
            case PRODUCTION_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              candie.setProduction(xmlEvent.asCharacters().getData());
              break;
            }
            case VALUE_TAG : {
              value = new Value();
              break;
            }
            case ENERGY_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              candie.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case TYPES_TAG: {
              type = new Type();
              break;
            }
            case CARAMEL_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setCaramel(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case CANDIE_GROUP_TAG: {
              candieGroup = new CandieGroup();
              Attribute id = startElement.getAttributeByName(new QName(CANDIE_GROUP_ID_ATTRIBUTE));
              candieGroup.setId(Integer.parseInt(id.getValue()));
              xmlEvent = xmlEventReader.nextEvent();
              candieGroup.setName(xmlEvent.asCharacters().getData());
              break;
            }
            case IRIS_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setIris(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case CHOCOLATE_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setChocolate(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case INGREDIENTS_TAG: {
              ingredients = new ArrayList<>();
              break;
            }
            case ISSTUFFED_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setStuffed(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case PROTEINS_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              value.setProteins(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case FATS_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              value.setFats(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case CARBOHYDRATES_TAG  : {
              xmlEvent = xmlEventReader.nextEvent();
              value.setCarbohydrates(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          String name = endElement.getName().getLocalPart();
          switch (name) {
            case CANDIE_TAG: {
              candies.add(candie);
              candie = null;
              break;
            }
            case TYPES_TAG: {
              candie.setType(type);
              type = null;
              break;
            }
            case VALUE_TAG: {
              candie.setValue(value);
              value = null;
              break;
            }
            case CANDIE_GROUP_TAG: {
              type.setCandieGroup(candieGroup);
              candieGroup = null;
              break;
            }
            case INGREDIENTS_TAG: {
              candie.setIngredients(ingredients);
              ingredients = null;
              break;
            }
          }
        }
      }
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return candies;
  }
}

