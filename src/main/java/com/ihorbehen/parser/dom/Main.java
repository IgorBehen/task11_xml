package com.ihorbehen.parser.dom;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.ihorbehen.Model.Candie;
import com.ihorbehen.xmlValidator.XmlValidator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Main {
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder documentBuilder;

    public static void main(String... args) {
        File xmlFile = new File("src\\main\\resources\\xml\\candies.xml");
        File xsdFile = new File("src\\main\\resources\\xml\\candiesXSD.xsd");

        Document document = null;
        try {
            documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        if (XmlValidator.validate(document, xsdFile)) {
            List<Candie> candieList = DOMParser.parse(document);
            System.out.println(candieList);
        } else {
            System.out.println("XML document failed validation.");
        }
    }
}
