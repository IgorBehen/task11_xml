package com.ihorbehen.Model;

import java.util.ArrayList;
import java.util.List;

public class Candie {
  private int candieId;
  private String name;
  private String production;
  private Value value;
  private Type type;
  private int energy;
  private List<Ingredient> ingredients = new ArrayList<>();

  public Candie(int deviceId, String name, String production, Value value, Type type, int energy, List<Ingredient> ingredients) {
    this.candieId = deviceId;
    this.name = name;
    this.production = production;
    this.value = value;
    this.type = type;
    this.energy = energy;
    this.ingredients = ingredients;
  }

  public Candie() {

  }

  public int getCandieId() {
    return candieId;
  }

  public void setCandieId(int candieId) {
    this.candieId = candieId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  public String getProduction() {
    return production;
  }

  public void setProduction(String production) {
    this.production = production;
  }

  public Value getValue() {
    return value;
  }

  public void setValue(Value value) {
    this.value = value;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public int getEnergy() {
    return energy;
  }

  public void setEnergy(int energy) {
    this.energy = energy;
  }
  public List<Ingredient> getIngredients() {
    return ingredients;
  }

  public void setIngredients(List<Ingredient> ingredients) {
    this.ingredients = ingredients;
  }

  @Override
  public String toString() {
    return "Candie{" +
        "deviceId=" + candieId +
        ", name='" + name + '\'' +
        ", production='" + production + '\'' +
        ", value=" + value +
        ", type=" + type +
        ", energy=" + energy +
        ", ingredients=" + ingredients +
        "}\n";
  }

  public void setValue(String price) {
  }
}
