<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEA">
        <div style="background-color: yellow; color: black;">
          <h2>C A N D I E S</h2>
        </div>
        <table border="3">
          <tr bgcolor="#fe2eb9">
            <th>Candie ID</th>
            <th>Name</th>
            <th>Energy</th>
            <th>Production</th>
            <th>Caramel</th>
            <th>Chocolate</th>
            <th>Stuffed</th>
            <th>Fats</th>
            <th>Carbohydrates</th>
            <th>Proteins</th>
          </tr>

          <xsl:for-each select="candies/candie">
            <tr>
              <td><xsl:value-of select="@candieId" /></td>
              <td><xsl:value-of select="name"/></td>
              <td>
                <xsl:value-of select="energy "/>
                <xsl:text> KKAL</xsl:text>
              </td>
              <td><xsl:value-of select="production"/></td>
              <td>
                <xsl:for-each select="types/caramel">
                  <xsl:value-of select="."/>
                  <xsl:text>, </xsl:text>
                </xsl:for-each>
                </td>
              <td>
                <xsl:value-of select="types/chocolate"/>
                <xsl:text>.</xsl:text>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/isStuffed ='true'">
                  <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:value-of select="value/fats"/>
                <xsl:text> gram</xsl:text>
              </td>
              <td>
                <xsl:value-of select="value/carbohydrates"/>
                <xsl:text> gram</xsl:text>
              </td>
              <td>
                <xsl:value-of select="value/proteins"/>
                <xsl:text> gram</xsl:text>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>