package com.ihorbehen.parser.sax;

import com.ihorbehen.Model.Candie;

import java.io.File;
import java.util.List;

public class Main {

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\xml\\candies.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\candiesXSD.xsd");

    List<Candie> candies = SaxParser.parse(xmlFile, xsdFile);
    System.out.println(candies);
  }
}
