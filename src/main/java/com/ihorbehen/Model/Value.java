package com.ihorbehen.Model;

public class Value {
    private CandieGroup candieGroup;
    private int proteins;
    private int fats;
    private int carbohydrates;

    public Value(int proteins, int fats, int carbohydrates) {
        this.candieGroup = candieGroup;
        this.proteins = proteins;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    public Value() {

    }

    public CandieGroup getCandieGroup() {
        return candieGroup;
    }

    public void setCandieGroup(CandieGroup candieGroup) {
        this.candieGroup = candieGroup;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public double getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Type{" +
                "proteins=" + proteins +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                '}';
    }
}
