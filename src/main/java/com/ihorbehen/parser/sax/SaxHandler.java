package com.ihorbehen.parser.sax;

import java.util.ArrayList;
import java.util.List;

import com.ihorbehen.Model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

  static final String CANDIE_TAG = "candie";
  static final String NAME_TAG = "name";
  static final String PRODUCTION_TAG = "production ";
  static final String VALUE_TAG = "value";
  static final String ENERGY_TAG = "energy";
  static final String TYPES_TAG = "types";
  static final String CARAMEL_TAG = "caramel";
  static final String CANDIE_GROUP_TAG = "candieGroup";
  static final String IRIS_TAG = "iris";
  static final String CHOCOLATE_TAG = "chocolate";
  static final String INGREDIENTS_TAG = "ingredients";
  static final String ISSTUFFED_TAG = "isStuffed";
  static final String CANDIE_ID_ATTRIBUTE = "candieId";
  static final String CANDIE_GROUP_ID_ATTRIBUTE = "id";
  static final String PROTEINS_TAG = "proteins";
  static final String FATS_TAG  = "fats";
  static final String CARBOHYDRATES_TAG  = "carbohydrates";

  private List<Candie> candies = new ArrayList<>();
  private Candie candie;
  private Type type;
  private Value value;
  private CandieGroup candieGroup;
  private List<Ingredient> ingredients;
  private String currentElement;

  public List<Candie> getCandieList() {
    return this.candies;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    currentElement = qName;

    switch (currentElement) {
      case CANDIE_TAG: {
        String candieId = attributes.getValue(CANDIE_ID_ATTRIBUTE);
        candie = new Candie();
        candie.setCandieId(Integer.parseInt(candieId));
        break;
      }
      case TYPES_TAG: {
        type = new Type();
        break;
      }
      case VALUE_TAG: {
        value = new Value();
        break;
      }
      case CANDIE_GROUP_TAG: {
        String candieGroupId = attributes.getValue(CANDIE_GROUP_ID_ATTRIBUTE);
        candieGroup = new CandieGroup();
        candieGroup.setId(Integer.parseInt(candieGroupId));
        break;
      }
      case INGREDIENTS_TAG: {
        ingredients = new ArrayList<>();
        break;
      }
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    switch (qName) {
      case CANDIE_TAG: {
        candies.add(candie);
        break;
      }
      case TYPES_TAG: {
        candie.setType(type);
        type = null;
        break;
      }
      case VALUE_TAG: {
        candie.setValue(value);
        value = null;
        break;
      }
      case CANDIE_GROUP_TAG: {
        type.setCandieGroup(candieGroup);
        candieGroup = null;
        break;
      }
      case INGREDIENTS_TAG: {
        candie.setIngredients(ingredients);
        ingredients = null;
        break;
      }
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (currentElement.equals(NAME_TAG)) {
      candie.setName(new String(ch, start, length));
    }
    if (currentElement.equals(PRODUCTION_TAG)) {
      candie.setProduction(new String(ch, start, length));
    }
    if (currentElement.equals(ENERGY_TAG)) {
      candie.setEnergy(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(CARAMEL_TAG)) {
      type.setCaramel(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(CANDIE_GROUP_TAG)) {
      candieGroup.setName(new String(ch, start, length));
    }
    if (currentElement.equals(IRIS_TAG)) {
      type.setIris(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(CHOCOLATE_TAG)) {
      type.setChocolate(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(ISSTUFFED_TAG)) {
      type.setStuffed(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(PROTEINS_TAG)) {
      value.setProteins(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(FATS_TAG)) {
      value.setFats(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(CARBOHYDRATES_TAG)) {
      value.setCarbohydrates(Integer.parseInt(new String(ch, start, length)));
    }
  }
}
